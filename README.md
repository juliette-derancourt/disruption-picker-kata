# Contexte fonctionnel

Le produit SIDH est le moteur d'itinéraires de OUI.SNCF. Afin d'anticiper les grèves à venir, nous souhaitons mettre en place une solution permettant de perturber les trains (annulation de train, ajouts, retards, ...) 
La première étape de cette nouvelle solution est de mettre en place un nouvel applicatif A.K.A. "Le DisruptionPicker" chargé de collecter et filtrer ces perturbations.

# Kata

Ce "DisruptionPicker" consomme des notifications de perturbations provenant d'une source de données externe.

### Domain

Les perturbations sont reçues via des notifications :
- une notification contient :
    - un type - exemple DEBUT/FIN/DATA
	- une perturbation
- une perturbation contient :
	- un numéro de train [obligatoire]
	- un code station d'origine sur 5 caractères - exemple (FRABC)	[obligatoire]
	- une perturbation associée à l'origine (qui est en réalité un statut "ADD", "MAJ" ou "SUP") [facultative]
	- un code station de destination sur 5 caractères - exemple (FRXYZ)	[obligatoire]
	- une perturbation associée à la destination (qui est en réalité un statut "ADD", "MAJ" ou "SUP") [facultative]
		
### Core

- Le produit peut recevoir plusieurs notifications pour un même numéro de train :
    - exemple : le train est retardé à l'origine et à la destination (statut "MAJ")
	- le produit va recevoir une première notification pour le retard à l'origine (> perturbation associée à l'origine valorisée dans la notification)
	- Puis, une deuxième notification pour le retard à la destination, comprenant donc également la perturbation de l'origine (> perturbation associée à l'origine et à la destination valorisées dans la notification)
- Le produit recevra une "notification de début d'envoi" l'informant que les notifications de perturbations vont arriver
- Il faudra retenir ces notification jusqu'à réception de la "notification de fin"
- Le produit ne doit retenir que la dernière notification reçue pour un numéro de train donné (en mode "annule et remplace")

### Finalité fonctionnelle

Les perturbations résultantes seront exposées pour un autre applicatif.

### Stack
Le choix précis des technos n'est pas encore fait, mais nous souhaiterions commencer l'implémentation en préservant tout de même notre socle technique.

Socle technique :
- JDK 12.0.2
- JUnit 5
- Maven

### Consignes techniques

Limiter l'utilisation de librairies. 

A vous de jouer
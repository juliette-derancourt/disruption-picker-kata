package disruptionpicker.domain;

import disruptionpicker.domain.api.DisruptionFinder;
import disruptionpicker.domain.api.DisruptionNotificationHandler;
import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;
import disruptionpicker.domain.spi.Disruptions;

import java.util.Optional;

public class DisruptionPicker implements DisruptionNotificationHandler, DisruptionFinder {

    private final Disruptions disruptions;

    public DisruptionPicker(Disruptions disruptions) {
        this.disruptions = disruptions;
    }

    @Override
    public void start(Disruption disruption) {
        disruptions.saveAndReplace(disruption);
    }

    @Override
    public void update(Disruption disruption) {
        disruptions.saveAndReplace(disruption);
    }

    @Override
    public void endDisruptionFor(TrainNumber trainNumber) {
        disruptions.removeDisruptionFor(trainNumber);
    }

    @Override
    public Optional<Disruption> findFor(TrainNumber trainNumber) {
        return disruptions.findBy(trainNumber);
    }

}
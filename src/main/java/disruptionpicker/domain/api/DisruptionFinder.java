package disruptionpicker.domain.api;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;

import java.util.Optional;

public interface DisruptionFinder {

    /**
     * Find the latest potential {@code Disruption} for the given train.
     *
     * @param trainNumber the number of the train to look up; never {@code null}
     * @return an {@code Optional} containing the current disruption for this
     * train; never {@code null} but potentially empty, if there is no disruption.
     */
    Optional<Disruption> findFor(TrainNumber trainNumber);

}
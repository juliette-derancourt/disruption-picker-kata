package disruptionpicker.domain.api;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;

public interface DisruptionNotificationHandler {

    /**
     * Starts a new {@code Disruption}.
     *
     * @param disruption the new disruption; never {@code null}
     */
    void start(Disruption disruption);

    /**
     * Updates the {@code Disruption} of a train.
     *
     * @param disruption the updated disruption; never {@code null}
     */
    void update(Disruption disruption);

    /**
     * Ends the disruption of the given {@code TrainNumber}.
     *
     * @param trainNumber the number of the disrupted train; never {@code null}
     */
    void endDisruptionFor(TrainNumber trainNumber);

}
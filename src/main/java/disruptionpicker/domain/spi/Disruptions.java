package disruptionpicker.domain.spi;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;

import java.util.Optional;

public interface Disruptions {

    void saveAndReplace(Disruption disruption);
    Optional<Disruption> findBy(TrainNumber trainNumber);
    void removeDisruptionFor(TrainNumber trainNumber);

}
package disruptionpicker.domain.model;

public enum DisruptionType {
    ADD, MAJ, SUP
}
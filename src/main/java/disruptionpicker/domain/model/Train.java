package disruptionpicker.domain.model;

import java.util.Objects;

public class Train {

    private final TrainNumber trainNumber;
    private final Station origin;
    private final Station destination;

    private Train(TrainNumber trainNumber, Station origin, Station destination) {
        this.trainNumber = trainNumber;
        this.origin = origin;
        this.destination = destination;
    }

    public static Train of(TrainNumber trainNumber, Station origin, Station destination) {
        if (origin.equals(destination)) {
            throw new IllegalArgumentException("Origin and destination must not be the same");
        }
        return new Train(trainNumber, origin, destination);
    }

    public TrainNumber getNumber() {
        return trainNumber;
    }

    Station getOrigin() {
        return origin;
    }

    Station getDestination() {
        return destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return Objects.equals(trainNumber, train.trainNumber) &&
                Objects.equals(origin, train.origin) &&
                Objects.equals(destination, train.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainNumber, origin, destination);
    }

}
package disruptionpicker.domain.model;

import java.util.Objects;
import java.util.Optional;

public class Disruption {

    private final Train train;
    private final DisruptionType disruptionAtOrigin;
    private final DisruptionType disruptionAtDestination;

    public Disruption(Train train, DisruptionType disruptionAtOrigin, DisruptionType disruptionAtDestination) {
        this.train = train;
        this.disruptionAtOrigin = disruptionAtOrigin;
        this.disruptionAtDestination = disruptionAtDestination;
    }

    Train getTrain() {
        return train;
    }

    public TrainNumber getTrainNumber() {
        return train.getNumber();
    }

    Optional<DisruptionType> getDisruptionAtOrigin() {
        return Optional.ofNullable(disruptionAtOrigin);
    }

    Optional<DisruptionType> getDisruptionAtDestination() {
        return Optional.ofNullable(disruptionAtDestination);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disruption that = (Disruption) o;
        return Objects.equals(train, that.train) &&
                disruptionAtOrigin == that.disruptionAtOrigin &&
                disruptionAtDestination == that.disruptionAtDestination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(train, disruptionAtOrigin, disruptionAtDestination);
    }

}
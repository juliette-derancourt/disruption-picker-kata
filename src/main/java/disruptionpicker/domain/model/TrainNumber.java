package disruptionpicker.domain.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrainNumber {

    private static final Pattern TRAIN_NUMBER_FORMAT = Pattern.compile("[0-9]{1,6}");

    private final String number;

    private TrainNumber(String number) {
        this.number = number;
    }

    public static TrainNumber of(String trainNumber) {
        if (isInvalid(trainNumber)) {
            throw new IllegalArgumentException(String.format("Train number must be a series of 1 to 6 digits, but was %s", trainNumber));
        }
        return new TrainNumber(trainNumber);
    }

    private static boolean isInvalid(String trainNumber) {
        if (trainNumber == null) {
            return true;
        }
        Matcher matcher = TRAIN_NUMBER_FORMAT.matcher(trainNumber);
        return !matcher.matches();
    }

    String get() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainNumber trainNumber = (TrainNumber) o;
        return Objects.equals(number, trainNumber.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

}
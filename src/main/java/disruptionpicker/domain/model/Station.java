package disruptionpicker.domain.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Station {

    private static final Pattern STATION_CODE_FORMAT = Pattern.compile("[A-Z]{5}");

    private final String code;

    private Station(String code) {
        this.code = code;
    }

    public static Station withCode(String stationCode) {
        if (isInvalid(stationCode)) {
            throw new IllegalArgumentException(String.format("Station code must be a series of 5 uppercase letters, but was %s", stationCode));
        }
        return new Station(stationCode);
    }

    private static boolean isInvalid(String stationCode) {
        if (stationCode == null) {
            return true;
        }
        Matcher matcher = STATION_CODE_FORMAT.matcher(stationCode);
        return !matcher.matches();
    }

    String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return Objects.equals(code, station.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

}
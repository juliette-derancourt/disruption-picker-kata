package disruptionpicker.infrastructure;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;
import disruptionpicker.domain.spi.Disruptions;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryDisruptionRepository implements Disruptions {

    private final Map<TrainNumber, Disruption> disruptions = new HashMap<>();

    @Override
    public void saveAndReplace(Disruption disruption) {
        disruptions.put(disruption.getTrainNumber(), disruption);
    }

    @Override
    public Optional<Disruption> findBy(TrainNumber trainNumber) {
        return Optional.ofNullable(disruptions.get(trainNumber));
    }

    @Override
    public void removeDisruptionFor(TrainNumber trainNumber) {
        disruptions.remove(trainNumber);
    }

}
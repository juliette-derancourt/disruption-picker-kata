package disruptionpicker.infrastructure;

public enum NotificationType {
    START, END, DATA
}
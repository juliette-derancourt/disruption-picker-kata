package disruptionpicker.infrastructure;

import disruptionpicker.domain.model.*;

public class DisruptionNotification {

    private NotificationType type;
    private String trainNumber;
    private String originStationCode;
    private String destinationStationCode;
    private DisruptionType disruptionAtOrigin;
    private DisruptionType disruptionAtDestination;

    public DisruptionNotification(NotificationType type, String trainNumber,
                                  String originStationCode, String destinationStationCode,
                                  DisruptionType disruptionAtOrigin, DisruptionType disruptionAtDestination) {
        this.type = type;
        this.trainNumber = trainNumber;
        this.originStationCode = originStationCode;
        this.destinationStationCode = destinationStationCode;
        this.disruptionAtOrigin = disruptionAtOrigin;
        this.disruptionAtDestination = disruptionAtDestination;
    }

    NotificationType getType() {
        return type;
    }

    TrainNumber getTrainNumber() {
        return TrainNumber.of(trainNumber);
    }

    Disruption toDisruption() {
        TrainNumber number = TrainNumber.of(trainNumber);
        Station origin = Station.withCode(originStationCode);
        Station destination = Station.withCode(destinationStationCode);
        Train train = Train.of(number, origin, destination);
        return new Disruption(train, disruptionAtOrigin, disruptionAtDestination);
    }

}
package disruptionpicker.infrastructure;

import disruptionpicker.domain.api.DisruptionNotificationHandler;

class DisruptionNotificationDispatcher {

    private final DisruptionNotificationHandler disruptionNotificationHandler;

    DisruptionNotificationDispatcher(DisruptionNotificationHandler disruptionNotificationHandler) {
        this.disruptionNotificationHandler = disruptionNotificationHandler;
    }

    void dispatch(DisruptionNotification notification) {
        switch (notification.getType()) {
            case START:
                disruptionNotificationHandler.start(notification.toDisruption());
                return;
            case DATA:
                disruptionNotificationHandler.update(notification.toDisruption());
                return;
            case END:
                disruptionNotificationHandler.endDisruptionFor(notification.getTrainNumber());
        }
    }

}
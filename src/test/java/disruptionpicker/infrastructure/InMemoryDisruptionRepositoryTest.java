package disruptionpicker.infrastructure;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.DisruptionType;
import disruptionpicker.domain.model.Train;
import disruptionpicker.domain.model.TrainNumber;
import disruptionpicker.utils.DisruptionResolver;
import disruptionpicker.utils.TrainResolver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith({DisruptionResolver.class, TrainResolver.class})
class InMemoryDisruptionRepositoryTest {

    private final InMemoryDisruptionRepository disruptionRepository = new InMemoryDisruptionRepository();

    @Test
    void should_save_disruption(Disruption disruption) {
        disruptionRepository.saveAndReplace(disruption);

        Optional<Disruption> saved = disruptionRepository.findBy(disruption.getTrainNumber());
        assertThat(saved).isPresent().contains(disruption);
    }

    @Test
    void should_save_disruption_and_replace_the_previous_ones(Train train) {
        Disruption disruption1 = new Disruption(train, DisruptionType.MAJ, null);
        disruptionRepository.saveAndReplace(disruption1);

        Disruption disruption2 = new Disruption(train, DisruptionType.MAJ, DisruptionType.MAJ);
        disruptionRepository.saveAndReplace(disruption2);

        Optional<Disruption> saved = disruptionRepository.findBy(train.getNumber());
        assertThat(saved).isPresent().contains(disruption2);
    }

    @Test
    void should_return_empty_if_there_is_no_disruption() {
        Optional<Disruption> disruption = disruptionRepository.findBy(TrainNumber.of("123"));
        assertThat(disruption).isEmpty();
    }

    @Test
    void should_remove_disruption(Disruption disruption) {
        disruptionRepository.saveAndReplace(disruption);

        TrainNumber trainNumber = disruption.getTrainNumber();
        disruptionRepository.removeDisruptionFor(trainNumber);

        assertThat(disruptionRepository.findBy(trainNumber)).isEmpty();
    }

}
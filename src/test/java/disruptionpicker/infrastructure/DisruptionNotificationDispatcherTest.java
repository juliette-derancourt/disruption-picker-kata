package disruptionpicker.infrastructure;

import disruptionpicker.domain.api.DisruptionNotificationHandler;
import disruptionpicker.utils.DisruptionNotificationResolver;
import disruptionpicker.utils.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static disruptionpicker.infrastructure.NotificationType.*;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class, DisruptionNotificationResolver.class})
class DisruptionNotificationDispatcherTest {

    @Mock
    private DisruptionNotificationHandler handler;

    private DisruptionNotificationDispatcher dispatcher;

    @BeforeEach
    void setUp() {
        dispatcher = new DisruptionNotificationDispatcher(handler);
    }

    @Test
    void should_dispatch_notification_with_START_type(@Type(START) DisruptionNotification notification) {
        dispatcher.dispatch(notification);

        verify(handler).start(notification.toDisruption());
    }

    @Test
    void should_dispatch_notification_with_DATA_type(@Type(DATA) DisruptionNotification notification) {
        dispatcher.dispatch(notification);

        verify(handler).update(notification.toDisruption());
    }

    @Test
    void should_dispatch_notification_with_END_type(@Type(END) DisruptionNotification notification) {
        dispatcher.dispatch(notification);

        verify(handler).endDisruptionFor(notification.getTrainNumber());
    }

}
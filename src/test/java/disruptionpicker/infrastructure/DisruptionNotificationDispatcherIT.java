package disruptionpicker.infrastructure;

import disruptionpicker.domain.DisruptionPicker;
import disruptionpicker.domain.api.DisruptionNotificationHandler;
import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.DisruptionType;
import disruptionpicker.domain.spi.Disruptions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.Optional;

import static disruptionpicker.domain.model.DisruptionType.MAJ;
import static disruptionpicker.infrastructure.NotificationType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
class DisruptionNotificationDispatcherIT {

    private DisruptionNotificationDispatcher dispatcher;
    private Disruptions disruptions;

    @BeforeAll
    void setUp() {
        disruptions = new InMemoryDisruptionRepository();
        DisruptionNotificationHandler handler = new DisruptionPicker(disruptions);
        dispatcher = new DisruptionNotificationDispatcher(handler);
    }

    @Test
    @Order(1)
    @DisplayName("The reception of a START notification should create a disruption for the given train number")
    void startDisruption() {
        DisruptionNotification notification = buildNotification(START, MAJ, null);
        dispatcher.dispatch(notification);

        assertThat(findDisruption(notification)).isPresent()
                .contains(notification.toDisruption());
    }

    @Test
    @Order(2)
    @DisplayName("The reception of a DATA notification should update the disruption related to the given train number")
    void updateDisruption() {
        DisruptionNotification notification = buildNotification(DATA, MAJ, MAJ);
        dispatcher.dispatch(notification);

        assertThat(findDisruption(notification)).isPresent()
                .contains(notification.toDisruption());
    }

    @Test
    @Order(3)
    @DisplayName("The reception of an END notification should end the disruption related to the given train number")
    void endDisruption() {
        DisruptionNotification notification = buildNotification(END, null, null);
        dispatcher.dispatch(notification);

        assertThat(findDisruption(notification)).isEmpty();
    }

    private DisruptionNotification buildNotification(NotificationType type, DisruptionType originDisruption, DisruptionType destinationDisrption) {
        return new DisruptionNotification(type, "123", "ABCDE", "FGHIJ", originDisruption, destinationDisrption);
    }

    private Optional<Disruption> findDisruption(DisruptionNotification notification) {
        return disruptions.findBy(notification.getTrainNumber());
    }

}
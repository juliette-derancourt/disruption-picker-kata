package disruptionpicker.utils;

import disruptionpicker.domain.model.Station;
import disruptionpicker.domain.model.Train;
import disruptionpicker.domain.model.TrainNumber;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class TrainResolver implements ParameterResolver {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == Train.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        TrainNumber trainNumber = TrainNumber.of("123");
        Station origin = Station.withCode("ABCDE");
        Station destination = Station.withCode("FGHIJ");
        return Train.of(trainNumber, origin, destination);
    }

}

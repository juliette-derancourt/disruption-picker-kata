package disruptionpicker.utils;

import disruptionpicker.domain.model.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class DisruptionResolver implements ParameterResolver {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == Disruption.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        TrainNumber trainNumber = TrainNumber.of("123");
        Station origin = Station.withCode("ABCDE");
        Station destination = Station.withCode("FGHIJ");
        Train train = Train.of(trainNumber, origin, destination);
        return new Disruption(train, null, DisruptionType.MAJ);
    }

}
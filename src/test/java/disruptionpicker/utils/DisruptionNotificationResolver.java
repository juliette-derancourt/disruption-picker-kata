package disruptionpicker.utils;

import disruptionpicker.infrastructure.DisruptionNotification;
import disruptionpicker.infrastructure.NotificationType;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import static disruptionpicker.domain.model.DisruptionType.MAJ;

public class DisruptionNotificationResolver implements ParameterResolver {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == DisruptionNotification.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        NotificationType type = getNotificationType(parameterContext);
        return new DisruptionNotification(type, "123", "ABCDE", "FGHIJ", MAJ, null);
    }

    private NotificationType getNotificationType(ParameterContext parameterContext) {
        return parameterContext.findAnnotation(Type.class)
                .map(Type::value)
                .orElse(NotificationType.DATA);
    }

}
package disruptionpicker.utils;

import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

public interface ValueObjectTest<T> {

    DifferentValues<T> provideDifferentValues();

    @Test
    default void should_be_equal_when_fields_are_equal() {
        Supplier<T> value = provideDifferentValues().first;
        assertThat(value.get()).isEqualTo(value.get());
        assertThat(value.get()).hasSameHashCodeAs(value.get());
    }

    @Test
    default void should_not_be_equal_when_fields_are_not_equal() {
        DifferentValues<T> values = provideDifferentValues();
        T value1 = values.first.get();
        T value2 = values.second.get();
        assertThat(value1).isNotEqualTo(value2);
        assertThat(value1.hashCode()).isNotEqualTo(value2.hashCode());
    }

    class DifferentValues<T> {

        Supplier<T> first;
        Supplier<T> second;

        private DifferentValues(Supplier<T> first, Supplier<T> second) {
            this.first = first;
            this.second = second;
        }

        public static <T> DifferentValues<T> of(Supplier<T> first, Supplier<T> second) {
            return new DifferentValues<>(first, second);
        }

    }

}

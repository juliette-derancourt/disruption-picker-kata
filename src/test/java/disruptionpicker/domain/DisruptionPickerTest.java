package disruptionpicker.domain;

import disruptionpicker.domain.model.Disruption;
import disruptionpicker.domain.model.TrainNumber;
import disruptionpicker.domain.spi.Disruptions;
import disruptionpicker.utils.DisruptionResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith({DisruptionResolver.class, MockitoExtension.class})
@DisplayName("The disruption picker")
class DisruptionPickerTest {

    private DisruptionPicker disruptionPicker;

    @Mock
    private Disruptions disruptions;

    @BeforeEach
    void setUp() {
        disruptionPicker = new DisruptionPicker(disruptions);
    }

    @Test
    @DisplayName("should handle the start of a train disruption")
    void should_start_disruption(Disruption disruption) {
        disruptionPicker.start(disruption);

        verify(disruptions).saveAndReplace(disruption);
        verifyNoMoreInteractions(disruptions);
    }

    @Test
    @DisplayName("should handle the update of a train disruption")
    void should_update_disruption(Disruption disruption) {
        disruptionPicker.update(disruption);

        verify(disruptions).saveAndReplace(disruption);
        verifyNoMoreInteractions(disruptions);
    }

    @Test
    @DisplayName("should handle the end of a train disruption")
    void should_end_disruption() {
        TrainNumber trainNumber = TrainNumber.of("123");
        disruptionPicker.endDisruptionFor(trainNumber);

        verify(disruptions).removeDisruptionFor(trainNumber);
        verifyNoMoreInteractions(disruptions);
    }

    @Test
    @DisplayName("should find an eventual disruption for a given train number")
    void should_find_latest_disruption(Disruption disruption) {
        TrainNumber trainNumber = TrainNumber.of("123");
        when(disruptions.findBy(trainNumber)).thenReturn(Optional.of(disruption));

        disruptionPicker.findFor(trainNumber);

        verify(disruptions).findBy(trainNumber);
        verifyNoMoreInteractions(disruptions);
    }

}
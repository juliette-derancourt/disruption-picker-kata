package disruptionpicker.domain.model;

import disruptionpicker.utils.ValueObjectTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;

class TrainTest implements ValueObjectTest<Train> {

    private final TrainNumber trainNumber = TrainNumber.of("123");
    private final Station origin = Station.withCode("ABCDE");
    private final Station destination = Station.withCode("FGHIJ");

    @Test
    void should_be_created_with_the_right_fields() {
        Train train = Train.of(trainNumber, origin, destination);
        assertAll(
                () -> assertThat(train.getNumber()).isEqualTo(trainNumber),
                () -> assertThat(train.getOrigin()).isEqualTo(origin),
                () -> assertThat(train.getDestination()).isEqualTo(destination)
        );
    }

    @Test
    void should_throw_an_exception_if_origin_and_destination_are_the_same() {
        String stationCode = "ABCDE";
        assertThatThrownBy(() -> Train.of(trainNumber, Station.withCode(stationCode), Station.withCode(stationCode)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Origin and destination must not be the same");
    }

    @Override
    public DifferentValues<Train> provideDifferentValues() {
        return DifferentValues.of(
                () -> Train.of(trainNumber, origin, destination),
                () -> Train.of(trainNumber, destination, origin)
        );
    }

}
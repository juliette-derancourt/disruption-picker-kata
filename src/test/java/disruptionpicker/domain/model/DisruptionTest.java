package disruptionpicker.domain.model;

import disruptionpicker.utils.TrainResolver;
import disruptionpicker.utils.ValueObjectTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@ExtendWith(TrainResolver.class)
class DisruptionTest implements ValueObjectTest<Disruption> {

    private Train train;

    @BeforeEach
    void setUp(Train train) {
        this.train = train;
    }

    @Test
    void should_be_created_with_the_right_fields() {
        Disruption disruption = new Disruption(train, null, DisruptionType.ADD);
        assertAll(
                () -> assertThat(disruption.getTrain()).isEqualTo(train),
                () -> assertThat(disruption.getDisruptionAtOrigin()).isEmpty(),
                () -> assertThat(disruption.getDisruptionAtDestination()).contains(DisruptionType.ADD)
        );
    }

    @Override
    public DifferentValues<Disruption> provideDifferentValues() {
        return DifferentValues.of(
                () -> new Disruption(train, DisruptionType.ADD, DisruptionType.SUP),
                () -> new Disruption(train, DisruptionType.MAJ, null)
        );
    }

}
package disruptionpicker.domain.model;

import disruptionpicker.utils.ValueObjectTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TrainNumberTest implements ValueObjectTest<TrainNumber> {

    @ParameterizedTest(name = "Valid: {0}")
    @ValueSource(strings = {"123", "7256", "42", "010101"})
    void should_be_created_if_the_train_number_is_valid(String number) {
        TrainNumber trainNumber = TrainNumber.of(number);
        assertThat(trainNumber.get()).isEqualTo(number);
    }

    @ParameterizedTest(name = "Invalid: {0}")
    @ValueSource(strings = {"abc", "12345678", "...", "?!"})
    @NullAndEmptySource
    void should_throw_an_exception_if_the_train_number_is_invalid(String trainNumber) {
        assertThatThrownBy(() -> TrainNumber.of(trainNumber))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("Train number must be a series of 1 to 6 digits");
    }

    @Override
    public DifferentValues<TrainNumber> provideDifferentValues() {
        return DifferentValues.of(
                () -> TrainNumber.of("123"),
                () -> TrainNumber.of("456")
        );
    }

}
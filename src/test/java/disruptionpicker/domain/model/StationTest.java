package disruptionpicker.domain.model;

import disruptionpicker.utils.ValueObjectTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class StationTest implements ValueObjectTest<Station> {

    @ParameterizedTest(name = "Valid: {0}")
    @ValueSource(strings = {"ABCDE", "ZZZZZ", "HELLO", "WORLD"})
    void should_be_created_if_the_station_code_is_valid(String stationCode) {
        Station station = Station.withCode(stationCode);
        assertThat(station.getCode()).isEqualTo(stationCode);
    }

    @ParameterizedTest(name = "Invalid: {0}")
    @ValueSource(strings = {"ABC", "abcde", "ABC1", "!"})
    @NullAndEmptySource
    void should_throw_an_exception_if_the_station_code_is_invalid(String stationCode) {
        assertThatThrownBy(() -> Station.withCode(stationCode))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageStartingWith("Station code must be a series of 5 uppercase letters");
    }

    @Override
    public DifferentValues<Station> provideDifferentValues() {
        return DifferentValues.of(
                () -> Station.withCode("ABCDE"),
                () -> Station.withCode("ZYXWV")
        );
    }

}